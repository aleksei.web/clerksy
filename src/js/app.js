import simpleParallax from 'simple-parallax-js';

const pItems = document.querySelectorAll('.get__decor');

new simpleParallax(pItems, {
  overflow: true
});

function dropDown() {
  const dropDown = document.querySelectorAll('.dropdown');
  if (dropDown) {
    dropDown.forEach(function (dropDownWrapper) {

      const dropDownBtn = dropDownWrapper.querySelector('.dropdown__btn');
      const dropDownBody = dropDownWrapper.querySelector('.dropdown__body');
      const dropDownItems = dropDownBody.querySelectorAll('.dropdown__item');
      const dropDownCurrent = dropDownWrapper.querySelector('.dropdown__current');
      const dropDownIcon = dropDownWrapper.querySelector('.dropdown__icon');
      const dropDownInput = dropDownWrapper.querySelector('.dropdown__input');

      dropDownBody.addEventListener('click', function (e) {
        e.stopPropagation();
      });

      dropDownBtn.addEventListener('click', function () {
        dropDownBody.classList.toggle('active');
        if (dropDownIcon) {
          dropDownIcon.classList.toggle('active');
        }
      });

      dropDownItems.forEach(function (item) {
        item.addEventListener('click', function () {
          dropDownCurrent.innerText = this.innerText;
          Array.from(dropDownItems).forEach(item => {
            item.classList.remove('active')
          });
          this.classList.add('active');

          dropDownInput.value = this.dataset.value;
          dropDownBody.classList.remove('active');
          if (dropDownIcon) {
            dropDownIcon.classList.remove('active');
          }
        })
      });

      document.addEventListener('click', function (e) {

        if (e.target !== dropDownBtn) {

          dropDownBody.classList.remove('active');
          if (dropDownIcon) {
            dropDownIcon.classList.remove('active');
          }
        }
      });

      document.addEventListener('keydown', function (e) {
        if (e.key === 'Tab' || e.key === 'Escape') {
          dropDownBody.classList.remove('active');
          if (dropDownIcon) {
            dropDownIcon.classList.remove('active');
          }
        }
      });

    });
  }
}
dropDown();
